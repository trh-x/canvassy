var path = require('path');
var webpack = require('webpack');
var npmPackage = require('./package.json');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');

var mainCSS = new ExtractTextPlugin(npmPackage.name + '.min.css');

module.exports = {
  devtool: 'source-map',
  entry: [
//    'webpack-hot-middleware/client',
    './src/app'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: npmPackage.name + '.min.js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    /*postLoaders: [
     * Commented out as breaking sourcemaps
     * https://github.com/webpack/transform-loader/issues/9
      {
        loader: 'transform?envify'
      }
    ],*/
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(lib|node_modules|bower_components)/,
        loaders: ['babel']
      },
      {
        test: /\.scss$/,
        loader: mainCSS.extract('style', 'css!postcss!sass?sourceMap')
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'file',
          'image-webpack'
        ]
      }
    ]
  },
  plugins: [
    mainCSS,
//    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  postcss: [autoprefixer()]
};
