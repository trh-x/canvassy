'use strict';

var path = require('path');
var npmPackage = require('./package.json');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');

var mainCSS = new ExtractTextPlugin(npmPackage.name + '.min.css');
var errorCSS = new ExtractTextPlugin(npmPackage.name + '-error.min.css');

module.exports = {
  entry: [
    './src/app'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: npmPackage.name + '.min.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    postLoaders: [
      {
        loader: 'transform?envify'
      }
    ],
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(lib|node_modules|bower_components)/,
        loaders: ['babel']
      },
      {
        test: /pub-cheers.scss$/,
        loader: mainCSS.extract('style', 'css!postcss-loader!sass')
      },
      {
        test: /pub-cheers-error.scss$/,
        loader: errorCSS.extract('style', 'css!postcss-loader!sass')
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'file?hash=sha512&digest=hex&name=[hash].[ext]',
          'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
        ]
      }
    ]
  },
  plugins: [
    mainCSS,
    errorCSS,
    new webpack.optimize.UglifyJsPlugin({
      compressor: { warnings: false }
    })
  ],
  postcss: [autoprefixer()]
};
