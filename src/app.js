import '../sass/_all.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import DrawingBoard from './components/DrawingBoard';

import configureStore from './store/configureStore';

const store = configureStore();

ReactDOM.render( <DrawingBoard store={store} />, document.getElementById('app') );
