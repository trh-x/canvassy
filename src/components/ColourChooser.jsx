import React from 'react';
//import ReactCSS from 'react-css';
import { connect } from 'react-redux';
import ColorPicker from 'react-color';
import { rgbToString } from '../drawing/colours';

function ColourChooser({ colour, isPickerVisible, setColour, setPickerVisible }) {
  return (
    <div>
      <button
        style={{backgroundColor: rgbToString(colour)}}
        onClick={() => setPickerVisible(!isPickerVisible)}
      >Pick</button>
      <ColorPicker
        type="sketch"
        display={isPickerVisible}
        color={colour}
        onChangeComplete={colour => setColour(colour.rgb)}
      />
    </div>
  );
}

ColourChooser.propTypes = {
  colour: React.PropTypes.shape({
    r: React.PropTypes.number.isRequired,
    g: React.PropTypes.number.isRequired,
    b: React.PropTypes.number.isRequired,
    a: React.PropTypes.number.isRequired,
  }).isRequired,
  isPickerVisible: React.PropTypes.bool.isRequired,
  setColour: React.PropTypes.func.isRequired,
  setPickerVisible: React.PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return state.foreground.toObject();
}

function mapDispatchToProps(dispatch) {
  return {
    setColour(colour) {
      dispatch({ type: 'COLOUR_CHANGE', colour })
    },

    setPickerVisible(isPickerVisible) {
      dispatch({ type: 'COLOUR_SET_PICKER_VISIBLE', isPickerVisible })
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ColourChooser);
