import R from 'ramda';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import { connect } from 'react-redux';

import { createLineStream } from '../drawing/streams/lineStream';
import { paintLine } from '../drawing/render/lines';

class Canvas extends React.Component {
  _paintCanvas() {
    const canvas = this.refs.canvas;
    console.log('_paintCanvas', canvas);
    this._ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (const line of this.props.lines.present) {
      paintLine(this._ctx, line);
    }
  }

  _subscribeToLines() {
    // TODO: Fix last-segment drawing preferably avoiding passing in `foreground`
    const lineStream = createLineStream(this.refs.canvas, this.props.foreground);
    this._subscription = lineStream.subscribe(line => this.props.onNewLine(line));
  }

  _resubscribeToLines() {
    this._subscription.dispose();
    this._subscribeToLines();
  }

  constructor() {
    super();

    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }

  componentDidMount() {
    this._ctx = this.refs.canvas.getContext('2d');
    this._paintCanvas();
    this._subscribeToLines();
  }

  componentDidUpdate() {
    this._paintCanvas();
    this._resubscribeToLines();
  }

  render() {
    console.log('render');
    return (
      <canvas ref="canvas" width="640" height="480"
      >Canvas not supported...</canvas>
    );
  }
}

// Canvas.propTypes = {

const mapStateToProps = R.identity;

const mapDispatchToProps = dispatch => ({
  onNewLine(line) {
    dispatch({ type: 'ADD_LINE', line });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Canvas);
