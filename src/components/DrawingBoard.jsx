import React from 'react';
import { Provider } from 'react-redux';
import DevTools from './instance/DevTools';
import ColourChooser from './ColourChooser';
import UndoRedo from './UndoRedo';
import Canvas from './Canvas';

function DrawingBoard({ store }) {
  return (
    <Provider store={store}>
      <main>
        <h1>Canvassy</h1>
        <ColourChooser />
        <UndoRedo />
        <Canvas />
        <DevTools />
      </main>
    </Provider>
  );
}

DrawingBoard.propTypes = { store: React.PropTypes.object.isRequired };

export default DrawingBoard;
