import React from 'react';
import { connect } from 'react-redux';
import { ActionCreators } from 'redux-undo';

function UndoRedo({ undo, redo, canUndo, canRedo }) {
  return (
    <div>
      <button onClick={undo} disabled={!canUndo}>Undo</button>
      <button onClick={redo} disabled={!canRedo}>Redo</button>
    </div>
  );
}

UndoRedo.propTypes = {
  undo: React.PropTypes.func.isRequired,
  redo: React.PropTypes.func.isRequired,
  canUndo: React.PropTypes.bool.isRequired,
  canRedo: React.PropTypes.bool.isRequired
};

const undo = ActionCreators.undo();
const redo = ActionCreators.redo();

function mapStateToProps(state) {
  return {
    canUndo: state.lines.past.length > 0,
    canRedo: state.lines.future.length > 0,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    undo() { dispatch(undo) },
    redo() { dispatch(redo) }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UndoRedo);
