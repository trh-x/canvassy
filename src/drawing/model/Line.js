export default class Line {
  constructor(colour) {
    this.points = [];
    this.colour = colour;
  }

  addPoint(point) {
    this.points.push(point);
  }

  // TODO: Implement iterator (maybe)
}
