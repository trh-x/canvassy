export function rgbToString({r, g, b, a}) {
  return `rgba(${r}, ${g}, ${b}, ${a})`;
}
