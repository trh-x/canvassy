import { rgbToString } from '../colours';

// TODO: animate line drawing?
function paintPointsWithColour(ctx, points, colour) {
  const numPoints = points.length;
  let [x, y] = points[0];

  const rgba = rgbToString(colour);

  if (numPoints === 1) {
    ctx.fillStyle = rgba;
    ctx.fillRect(x, y, 1, 1);
    return;
  }

  ctx.strokeStyle = rgba;
  ctx.beginPath();
  ctx.moveTo(x, y);
  for (let i = 1; i < numPoints; i++) {
    [x, y] = points[i];
    ctx.lineTo(x, y);
  }
  //ctx.closePath();
  ctx.stroke();
}

export function paintLine(ctx, line) {
  paintPointsWithColour(ctx, line.points, line.colour);
}

export function paintLastSegment(ctx, line) {
  let points = line.points;
  if (points.length > 1) points = points.slice(points.length - 2);
  paintPointsWithColour(ctx, points, line.colour);
}
