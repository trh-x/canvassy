import Rx from 'rx-dom';
import R from 'ramda';

import Line from '../model/Line';
import { paintLastSegment } from '../render/lines';

function getCoordsTranslate(canvas) {
  const {top, left} = canvas.getBoundingClientRect();
  console.log('getBoundingClientRect', top, left);
  return event => [event.pageX - left, event.pageY - top];
}

function debugSubscribe(message, observable) { observable.subscribe(e => console.log(message, e)); }

function consumeEvent(event) {
  event.stopPropagation();
  event.preventDefault();
  return event;
}

// TODO: Fix last-segment drawing preferably avoiding passing in `foreground`
export function createLineStream(canvas, foreground) {
  const ctx = canvas.getContext('2d');
  const getCoords = getCoordsTranslate(canvas);

  const mouseDown = Rx.DOM.mousedown(canvas).map(getCoords);
  const mouseMove = Rx.DOM.mousemove(canvas).map(getCoords);
  const mouseEnd  = Rx.DOM.mouseup(canvas).merge(Rx.DOM.mouseleave(canvas));

  const getTouchCoords = R.compose(getCoords, R.nth(0), R.prop('targetTouches'), consumeEvent);
  const touchStart = Rx.DOM.touchstart(canvas).map(getTouchCoords);
  const touchMove  = Rx.DOM.touchmove(canvas).map(getTouchCoords);
  const touchEnd   = Rx.DOM.touchend(canvas).merge(Rx.DOM.touchcancel(canvas));

  const lineStart = mouseDown.merge(touchStart);
  const lineMove  = mouseMove.merge(touchMove);
  const lineEnd   = mouseEnd.merge(touchEnd);

  //debugSubscribe('touchStart', touchStart);

  return lineStart.flatMap(point => {
    return Rx.Observable.just(point)
      .concat(lineMove.takeUntil(lineEnd))
      .reduce((line, point) => {
        line.addPoint(point);

        // TODO: Avoid side-effect in reduce... (see below)
        paintLastSegment(ctx, line);
        return line;
      }, new Line(foreground.get('colour')));
  });
}

  //.subscribe(x => console.log(x));

  /*const pointStreams = mouseDown.map(point => {
    return Rx.Observable.just(point)
      .concat(mouseMove.takeUntil(mouseEnd));
  });

  const publishedPointStreams = pointStreams.publish(); // Does it need publish()?
  publishedPointStreams.subscribe(pointStream => {
    pointStream.scan((points, point) => {
      if (points.length) return points = [points[1], point];
      return [point];
    }, [])
    .subscribe(points => paintLastSegment(ctx, points));
  });

  return publishedPointStreams.map(pointStream => pointStream.reduce((line, point) => {
    line.push(point);
    return line;
  }, []));*/
