import { createStore, compose } from 'redux'
//import { createStore, applyMiddleware, compose } from 'redux'
import { persistState } from 'redux-devtools';

import DevTools from '../components/instance/DevTools';
import rootReducer from './reducers/rootReducer';

function getDebugSessionKey() { return 'redux-foo'; }

// TODO: Redux Saga, Relay & GraphQL for API
// React Router + react-router-redux

const middleware = compose(DevTools.instrument(), persistState(getDebugSessionKey()));

export default function configureStore() {
  return createStore(
    rootReducer,
    middleware
  );
};
