import { combineReducers } from 'redux';
import undoable, { distinctState } from 'redux-undo';
import Immutable from 'immutable';

/*const initialState = Immutable.fromJS({
  lines: []
});*/
// state.updateIn(['lines'], lines => lines.push(action.line))

function lines(state = new Immutable.List(), action) {
  switch (action.type) {
    case 'ADD_LINE':
      return state.push(action.line);
    default:
      return state;
  }
}

const initialForeground = new Immutable.Map({
  colour: { r: 0, g: 0, b: 0, a: 1 },
  isPickerVisible: false
});

function colour(state = initialForeground, action) {
  switch (action.type) {
    case 'COLOUR_CHANGE':
      return state.set('colour', action.colour);
    case 'COLOUR_SET_PICKER_VISIBLE':
      return state.set('isPickerVisible', action.isPickerVisible);
    default:
      return state;
  }
}

const undoableLines = undoable(lines, { filter: distinctState() });

export default combineReducers({ lines: undoableLines, foreground: colour });
